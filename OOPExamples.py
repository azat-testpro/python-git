
class Name():

    name = ""

    def get_name(self):
        return self.name

    def set_name(self, given_name):
       self.name = given_name

    def print_name(self):
        print self.name


    #Static
    @staticmethod
    def print_random_String():
        print "qwerty"

Name.print_random_String()

person = Name()
person.set_name("Azat")
azat = person.get_name()


# print azat



