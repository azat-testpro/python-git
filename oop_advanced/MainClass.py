class Car():

    wheels = 4

    def get_wheels(self):
        return self.wheels

    def has_engine(self):
        return True

    def get_country(self):
        return "USA"

    def get_logo(self):
        return "Standart logo"

class BMW(Car):

    def get_country(self):
        return "German"

    def get_logo(self):
        return "BMW logo"

    def print_bmw(self):
        print "BMW"

class Audi(Car):

    def get_country(self):
        return "France"

    def get_logo(self):
        return "Audi logo"

my_car = BMW()
# print my_car.get_logo()