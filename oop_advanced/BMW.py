from oop_advanced.Car import Car


class BMW(Car):

    def get_country(self):
        return "German"

    def get_logo(self):
        return "BMW logo"